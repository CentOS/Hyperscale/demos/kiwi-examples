# Project examples for CentOS image builds with KIWI

This repository contains examples for building CentOS images with the [KIWI image builder](https://osinside.github.io/kiwi).

The KIWI descriptions are stored in the `*-build/` subdirectories.
Refer to the [KIWI documentation](https://osinside.github.io/kiwi/) for more details.

## Appliance build quick start

### Podman/Docker

The instructions below will use the `podman` command, but you can easily substitute for `docker`
and it should easily just work the same.

First, pull down the container of the required environment (Fedora 34 or newer or CentOS/RHEL 9 or newer).

For the instructions below, we'll work with CentOS Stream Hyperscale 9.

```bash
$ sudo podman pull quay.io/centoshyperscale/centos:stream9
```

Assuming you're in the root directory of the Git checkout, set up the container:

```bash
$ sudo podman run --privileged --rm -it -v $PWD:/code quay.io/centoshyperscale/centos:stream9 /bin/bash
```

Once in the container environment, set up your development environment and run the image build (we're using the `trivial-build` for this example):

```bash
# Change to the code location
[]$ cd /code
# Install kiwi
[]$ dnf --assumeyes install kiwi
# Run the image build
[]$ kiwi-ng system build --description ./trivial-build --target-dir ./tmpoutput
```

### VM/Bare metal

Assuming you're using CentOS Stream 9, get KIWI installed first:

1. [Enable EPEL](https://docs.fedoraproject.org/en-US/epel/#_el9)
2. Install `kiwi`

Then you can run an image build by running the following (we using `trivial-build` for this example):

```bash
[]$ sudo kiwi-ng system build --description ./trivial-build --target-dir ./tmpoutput`
```

### Note for `complex-build`

The `complex-build` description uses the KIWI profile feature to define multiple artifact types in a singular description.
To build artifacts from that description, use the following command structure instead:

```bash
[]$ sudo kiwi-ng --type=<type> --profile=<profile> system build --description ./complex-build --target-dir ./tmpoutput
```

For example, to build an OpenStack image, run the following command:

```bash
[]$ sudo kiwi-ng --type=oem --profile=OpenStack system build --description ./complex-build --target-dir ./tmpoutput
```

## Licensing

These descriptions are licensed under the Apache Software License, version 2.0. See `LICENSE` for details.
