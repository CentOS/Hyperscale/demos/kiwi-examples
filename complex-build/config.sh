#!/bin/bash

set -euxo pipefail

#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$kiwi_iname]-[$kiwi_profiles]..."

#======================================
# Turn on sticky vendors
#--------------------------------------
echo "allow_vendor_change=True" >> /etc/dnf/dnf.conf

#======================================
# Clear machine specific configuration
#--------------------------------------
## Force generic hostname
echo "localhost" > /etc/hostname
## Clear machine-id on pre generated images
truncate -s 0 /etc/machine-id

#======================================
# Delete & lock the root user password
#--------------------------------------
if [[ "$kiwi_profiles" == *"AWS"* ]] || [[ "$kiwi_profiles" == *"Azure"* ]] || [[ "$kiwi_profiles" == *"OpenStack"* ]] || [[ "$kiwi_profiles" == *"Live"* ]]; then
	passwd -d root
	passwd -l root
fi

#======================================
# Setup default services
#--------------------------------------

if [[ "$kiwi_profiles" == *"AWS"* ]] || [[ "$kiwi_profiles" == *"Azure"* ]] || [[ "$kiwi_profiles" == *"OpenStack"* ]]; then
	## Enable cloud-init
	systemctl enable cloud-config.service cloud-final.service cloud-init.service cloud-init-local.service cloud-init.target
fi

if [[ "$kiwi_profiles" == *"Azure"* ]]; then
	## Enable WALinuxAgent
	systemctl enable waagent.service
fi

## Enable chrony
systemctl enable chronyd.service
## Enable oomd
systemctl enable systemd-oomd.service
## Enable resolved
systemctl enable systemd-resolved.service
## Enable persistent journal
mkdir -p /var/log/journal

#======================================
# Setup default target
#--------------------------------------
systemctl set-default multi-user.target

exit 0
